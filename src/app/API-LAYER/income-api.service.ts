import { Injectable } from '@angular/core';
import{HttpClient}from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class IncomeApiService {
api_url="http://localhost:3000/income_transactions"
  constructor(private http:HttpClient) { }
getAllIncomeExpences()
{
  return this.http.get(this.api_url)
}
insertNewIncomeTransaction(formdata:any)
{
  return this.http.post(this.api_url,formdata)
}
updateIncomeTransaction(id:any,formdata:any)
{
let url=this.api_url+'/'+id;
return  this.http.put(url,formdata)
}
deleteIncomeTransaction(id:any)
{
let url=this.api_url+'/'+id;
return this.http.delete(url)

}
}
