import { TestBed } from '@angular/core/testing';

import { ExpencesApiService } from './expences-api.service';

describe('ExpencesApiService', () => {
  let service: ExpencesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExpencesApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
