import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
 @Injectable({
  providedIn: 'root'
})
export class ExpencesApiService {
api_url="http://localhost:3000/expences_transactions"
  constructor(private http:HttpClient) { }

  getAllExpences()
  {
    return this.http.get(this.api_url)
  }
  getAllExpencesByType(type:any)
  {
let url=this.api_url+'?type='+type
return this.http.get(url)
  }
  getAllExpencesById(id:any)
  {
let url=this.api_url+'/'+id
return this.http.get(url)
  }
  insertNewExpences(formdata:any)
  {
    return this.http.post(this.api_url,formdata)
  }
  updateExpences(id:any,formdata:any)
  {
let url=this.api_url+'/'+id
return this.http.put(url,formdata)
  }
  deleteExpences(id:any)
  {
    let url=this.api_url+'/'+id
    return this.http.delete(url)
  }
}
