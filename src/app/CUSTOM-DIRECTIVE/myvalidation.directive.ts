import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMyvalidation]'
})
export class MyvalidationDirective {

  constructor(private el:ElementRef) { 
    this.el.nativeElement.style.color="blue"
    this.el.nativeElement.innerText="This field is mandatory"
    // this.el.nativeElement.style.backgroundColor="wheat"
  }

}
