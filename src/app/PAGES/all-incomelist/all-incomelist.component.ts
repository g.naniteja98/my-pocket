import { Component, OnInit } from '@angular/core';
import { IncomeApiService } from 'src/app/API-LAYER/income-api.service';

@Component({
  selector: 'app-all-incomelist',
  templateUrl: './all-incomelist.component.html',
  styleUrls: ['./all-incomelist.component.css']
})
export class AllIncomelistComponent implements OnInit {
  income_list: any = []
  total_income = 0
  total_exp: any
  p: number = 1;
  constructor(private income_api: IncomeApiService) { }

  ngOnInit(): void {
    this.displayallincomelist()
  }

  displayallincomelist() {
    this.total_income = 0
    this.income_api.getAllIncomeExpences().subscribe(res => {
      this.income_list = res
      for (let index = 0; index < this.income_list.length; index++) {
        this.total_income = this.total_income + this.income_list[index].amount
      }
      localStorage.setItem('all_income', JSON.stringify(this.total_income))

    })
  
    this.total_exp = localStorage.getItem("all_exp")
  }

}
