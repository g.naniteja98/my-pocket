import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllIncomelistComponent } from './all-incomelist.component';

describe('AllIncomelistComponent', () => {
  let component: AllIncomelistComponent;
  let fixture: ComponentFixture<AllIncomelistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllIncomelistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllIncomelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
