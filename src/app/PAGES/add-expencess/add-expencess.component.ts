import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ExpencesApiService } from 'src/app/API-LAYER/expences-api.service';

@Component({
  selector: 'app-add-expencess',
  templateUrl: './add-expencess.component.html',
  styleUrls: ['./add-expencess.component.css']
})
export class AddExpencessComponent implements OnInit {
  txt_reason: any
  txt_amount: any
  txt_type: any
  txt_date: any
  selectedId: any
  selected_data: any
  constructor(private exp_api: ExpencesApiService, private myrouter: ActivatedRoute,
    private router:Router,private toastr:ToastrService
    ) { }

  ngOnInit(): void {
    debugger;
    this.selectedId = this.myrouter.snapshot.params["id"]
    this.exp_api.getAllExpencesById(this.selectedId).subscribe((res: any) => {
      this.selected_data = res;
      this.txt_reason = res.reason
      this.txt_amount = res.amount
      this.txt_type = res.type
      this.txt_date = res.date
    })
    // alert(this.selectedId)
  }
  save(f2: any) {
    let body = {
      "reason": f2.value.txt_reason,
      "amount": f2.value.txt_amount,
      "type": f2.value.txt_type,
      "date": f2.value.txt_date
    }
    this.exp_api.insertNewExpences(body).subscribe(res => {
      this.toastr.info("expences saved succussfully")
      f2.reset()
      this.router.navigate(["/all-transactions/all-expenceslist"])
    })
  }
update(f2:any)
{
  let body = {
    "reason": f2.value.txt_reason,
    "amount": f2.value.txt_amount,
    "type": f2.value.txt_type,
    "date": f2.value.txt_date
  }
  this.exp_api.updateExpences(this.selectedId,body).subscribe(res => {
    this.toastr.warning("expences updated succussfully")
    f2.reset()
    this.router.navigate(["/all-transactions/all-expenceslist"])
  })

}

}
