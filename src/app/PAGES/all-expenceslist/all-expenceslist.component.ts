import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ExpencesApiService } from 'src/app/API-LAYER/expences-api.service';

@Component({
  selector: 'app-all-expenceslist',
  templateUrl: './all-expenceslist.component.html',
  styleUrls: ['./all-expenceslist.component.css']
})
export class AllExpenceslistComponent implements OnInit {
  expences_list: any = []
  total_exp = 0
  p: number = 1;
  txt_type:any="All"

  id: any
  constructor(private exp_api: ExpencesApiService, private myrouter: Router,private toastr:ToastrService) { }

  ngOnInit(): void {

    this.displayAllExp()


  }
  displayAllExp() {
    this.total_exp = 0
    this.exp_api.getAllExpences().subscribe(res => {
      this.expences_list = res
      for (let index = 0; index < this.expences_list.length; index++) {
        this.total_exp = this.total_exp + this.expences_list[index].amount
      }
      localStorage.setItem("all_exp",JSON.stringify (this.total_exp))
      

    })

  }
  delete(id: any) {
    this.exp_api.deleteExpences(id).subscribe(res => {
      this.toastr.error("record deleted successfully")
      this.displayAllExp()
    })
  }
  edit(id: any) {
    this.myrouter.navigate(['edit-expence', id])
  }

  filterrecods(txt_type:any)
  {
    if(txt_type == "All")
    {
this.displayAllExp()
    }
    else{
      this.exp_api.getAllExpencesByType(txt_type).subscribe(res=>{
        this.expences_list=res;
      })
    }

  }
}
