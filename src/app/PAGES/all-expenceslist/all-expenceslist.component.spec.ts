import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllExpenceslistComponent } from './all-expenceslist.component';

describe('AllExpenceslistComponent', () => {
  let component: AllExpenceslistComponent;
  let fixture: ComponentFixture<AllExpenceslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllExpenceslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllExpenceslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
