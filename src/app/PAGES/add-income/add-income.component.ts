import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IncomeApiService } from 'src/app/API-LAYER/income-api.service';

@Component({
  selector: 'app-add-income',
  templateUrl: './add-income.component.html',
  styleUrls: ['./add-income.component.css']
})
export class AddIncomeComponent implements OnInit {
  txt_source:any
  txt_amount:any
  constructor(private income_api:IncomeApiService,private router:Router,private toastr:ToastrService) { }

  ngOnInit(): void {
  }
save(f1:any)
{
let body={
      "amount":f1.value.txt_amount,
      "income_source":f1.value.txt_source,
      "date":new Date().toUTCString()
}
this.income_api.insertNewIncomeTransaction(body).subscribe(res=>{
  this.toastr.success("income saved succesfully")
  f1.reset()
  this.router.navigate(["/all-transactions/all-incomelist"])
})
}
}
