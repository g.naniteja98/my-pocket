import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WellcomepageComponent } from './wellcomepage.component';

describe('WellcomepageComponent', () => {
  let component: WellcomepageComponent;
  let fixture: ComponentFixture<WellcomepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WellcomepageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WellcomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
