import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule}from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IncomeApiService } from './API-LAYER/income-api.service';
import { HomepageComponent } from './PAGES/homepage/homepage.component';
import { AddIncomeComponent } from './PAGES/add-income/add-income.component';
import { AddExpencessComponent } from './PAGES/add-expencess/add-expencess.component';
import { AllTransactionsComponent } from './PAGES/all-transactions/all-transactions.component';
import { AllIncomelistComponent } from './PAGES/all-incomelist/all-incomelist.component';
import { AllExpenceslistComponent } from './PAGES/all-expenceslist/all-expenceslist.component';
import { WellcomepageComponent } from './PAGES/wellcomepage/wellcomepage.component';
import{FormsModule}from '@angular/forms' 
import { ExpencesApiService } from './API-LAYER/expences-api.service';
import { MyvalidationDirective } from './CUSTOM-DIRECTIVE/myvalidation.directive';
import { DashboardComponent } from './PAGES/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {NgxPaginationModule} from 'ngx-pagination';
 @NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    AddIncomeComponent,
    AddExpencessComponent,
    AllTransactionsComponent,
    AllIncomelistComponent,
    AllExpenceslistComponent,
    WellcomepageComponent,
    MyvalidationDirective,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxPaginationModule
    ],
  providers: [IncomeApiService,ExpencesApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
