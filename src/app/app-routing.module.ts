import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddIncomeComponent } from './PAGES/add-income/add-income.component';
import { AddExpencessComponent } from './PAGES/add-expencess/add-expencess.component';
import { AllTransactionsComponent } from './PAGES/all-transactions/all-transactions.component';
import { AllIncomelistComponent } from './PAGES/all-incomelist/all-incomelist.component';
import { AllExpenceslistComponent } from './PAGES/all-expenceslist/all-expenceslist.component';
import { WellcomepageComponent } from './PAGES/wellcomepage/wellcomepage.component';
import { DashboardComponent } from './PAGES/dashboard/dashboard.component';

const routes: Routes = [
  {path:'add-income',component:AddIncomeComponent},
  {path:'dashboard',component:DashboardComponent},
  {path:'add-expence',component:AddExpencessComponent},
  {path:'edit-expence/:id',component:AddExpencessComponent},

  {path:'',component:WellcomepageComponent},
  {path:'all-transactions',component:AllTransactionsComponent,children:[
    {path:'all-incomelist',component:AllIncomelistComponent},
    {path:'all-expenceslist',component:AllExpenceslistComponent}


  ]},
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
